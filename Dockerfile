# checkov:skip=CKV_DOCKER_2: "Ensure that HEALTHCHECK instructions have been added to container images"
# This dockerfile is used for CI jobs only. The reason we don't have a HEALTHCHECK is that the runner
# executing the job itself is kind of doing health check by running the job
# checkov:skip=CKV_DOCKER_3: "Ensure that a user for the container has been created"
# This dockerfile runs in CI and we need to run as root as we write to /var/run locations for safety

ARG GL_ASDF_TERRAFORM_VERSION
ARG GL_ASDF_GLAB_VERSION

FROM hashicorp/terraform:${GL_ASDF_TERRAFORM_VERSION} as terraform

FROM registry.gitlab.com/gitlab-org/cli:v${GL_ASDF_GLAB_VERSION} as glab

FROM google/cloud-sdk:slim

COPY --from=terraform /bin/terraform /usr/local/bin/terraform

COPY --from=glab /usr/bin/glab /usr/local/bin/glab

RUN mkdir /opt/runway
COPY reconciler/ /opt/runway/reconciler/
RUN apt-get install -y idn2 jq unzip

RUN curl -o /usr/bin/gitlab-terraform https://gitlab.com/gitlab-org/terraform-images/-/raw/master/src/bin/gitlab-terraform.sh
RUN chmod +x /usr/bin/gitlab-terraform

WORKDIR /opt/runway/reconciler

ENTRYPOINT []
